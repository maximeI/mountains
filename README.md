# LesAlpes ![bluid](https://gitlab.com/maximeI/mountains/badges/master/pipeline.svg)

## Dependencies

* libglm-dev
* libglew-dev
* qt5-default

## Pour compiler, il faut OpenGL 3 et qmake  et executer la commande suivante:

```qmake && make```

## Pour lancer il faut executer la commande:

```./lesalpes```
